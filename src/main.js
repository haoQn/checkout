// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import * as VueGoogleMaps from "vue2-google-maps";
import ResetInput from 'v-reset-input'


Vue.use(ResetInput)
Vue.use(VueMaterial)
Vue.use(VueAxios, axios)
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyDtqlBL2XudSG3aIwHNNkBcj37CSjrFXqc",
    libraries: "places"
  }
});
var filter = function(text, length, clamp){
  clamp = clamp || '...';
  var node = document.createElement('div');
  node.innerHTML = text;
  var content = node.textContent;
  return content.length > length ? content.slice(0, length) + clamp : content;
};

Vue.filter('truncate', filter);
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
