import Vue from 'vue'
import Router from 'vue-router'
import Cart from '@/components/Cart'
import Product from '@/components/Product'
import Checkout from '@/components/Checkout'
import ThankYou from '@/components/ThankYou'
Vue.use(Router)

export default new Router({
  mode:"history",
  routes: [
    {
      path: '/cart',
      name: 'Cart',
      component: Cart
    },
    {
      path: '/product',
      name: 'Product',
      component: Product
    },
    {
      path: '/ThankYou',
      name: 'ThankYou',
      component: ThankYou
    }
  ]
})
